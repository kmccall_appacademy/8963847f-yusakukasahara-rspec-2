def reverser (&prc)
  sentence = prc.call
  words = sentence.split
  words.map { |word| word.reverse }.join(" ")
end

# puts reverser {"hello"}
# puts reverser {"hello world"}

def adder (default = 1, &prc)
  default + prc.call
end

# puts adder(3) {5}

def repeater (default = 1, &prc)
  default.times do prc.call
  end
end

# puts repeater(3) {n += 1}
