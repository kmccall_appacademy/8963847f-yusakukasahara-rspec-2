def measure(default = 1, &prc)
  start_time = Time.now
  default.times { prc.call }
  (Time.now - start_time) / default
end
